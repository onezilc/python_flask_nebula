import json
from flask import request, jsonify, send_from_directory, send_file, make_response

from src import common
from utils.initApp import mysql, default_response
from utils.checkAuth import check_auth

from . import files
from .service import FilesService


@files.route('/upload', methods=['POST'])
# @check_auth
def upload():

    values = dict()
    values["md5"] = request.form["md5"]
    values["type"] = request.form["type"]
    values["table"] = request.form["table"]
    values["name"] = request.form["name"]
    values["parentId"] = request.form["parentId"]
    values["size"] = request.form["size"]
    values["index"] = request.form["index"]
    values["chunkSize"] = request.form["chunkSize"]
    values["flag"] = request.form["flag"] or common.create_id()
    values["userId"] = common.get_token()["userId"]

    result = filesService.upload(request.files["file"], values)
    return default_response.construct_response(result)


@files.route('/download/<file_id>', methods=['GET'])
# @check_auth
def download(file_id):

    values = dict()
    values["id"] = file_id
    values["ranges"] = common.get_ranges()

    result, code = filesService.download(values)
    return default_response.construct_file_response(result), code


@files.route('/create', methods=['PUT'])
@check_auth
def create():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values["name"] = data["name"]
    values["parentId"] = data["parentId"]
    values["id"] = common.create_id()
    values["userId"] = common.get_token()["userId"]

    result, msg, code = filesService.create(values)
    return default_response.construct_response(result, msg, code)


@files.route('/listAll', methods=['GET'])
@check_auth
def list_all():

    values = dict()
    values['parentId'] = request.args.get('parentId') or '-1'
    values['order'] = request.args.get('order') or 'updateTime'
    values['sort'] = request.args.get('sort') or 'asc'
    values['name'] = request.args.get('name')
    values['type'] = request.args.get('type')
    values['userId'] = common.get_token()["userId"]

    result = filesService.list_all(values)
    return default_response.construct_response(result)


@files.route('/listGarbage', methods=['GET'])
@check_auth
def list_garbage():

    values = dict()
    values['parentId'] = request.args.get('parentId') or '-1'
    values['order'] = request.args.get('order') or 'updateTime'
    values['sort'] = request.args.get('sort') or 'asc'
    values['name'] = request.args.get('name')
    values['type'] = request.args.get('type')
    values['userId'] = common.get_token()["userId"]

    result = filesService.list_garbage(values)
    return default_response.construct_response(result)


@files.route('/rename', methods=['POST'])
@check_auth
def rename():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['parentId'] = data['parentId']
    values['id'] = data['id']
    values['name'] = data['name']

    result, msg, code = filesService.rename(values)
    return default_response.construct_response(result, msg, code)


@files.route('/delete', methods=['DELETE'])
@check_auth
def delete():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['ids'] = data['ids']

    result = filesService.delete(values)
    return default_response.construct_response(result)


@files.route('/remove', methods=['DELETE'])
@check_auth
def remove():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['ids'] = data['ids']

    result = filesService.remove(values)
    return default_response.construct_response(result)


@files.route('/move', methods=['POST'])
@check_auth
def move():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['targetId'] = data['targetId']
    values['ids'] = data['ids']

    result, msg, code = filesService.move(values)
    return default_response.construct_response(result, msg, code)


@files.route('/recover', methods=['POST'])
@check_auth
def recover():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['targetId'] = data['targetId']
    values['ids'] = data['ids']

    result = filesService.recover(values)
    return default_response.construct_response(result)


@files.route('/detail/<file_id>', methods=['GET'])
@check_auth
def detail(file_id):

    values = dict()
    values["id"] = file_id

    result = filesService.detail(values)
    return default_response.construct_response(result)


@files.route('/copy', methods=['POST'])
@check_auth
def copy():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values["targetId"] = data['parentId']
    values["id"] = data['fileId']
    values["userId"] = common.get_token()["userId"]

    result = filesService.copy(values)
    return default_response.construct_response(result)


@files.route('/statistics', methods=['GET'])
@check_auth
def statistics():

    values = dict()
    values['userId'] = common.get_token()["userId"]

    result = filesService.statistics(values)
    return default_response.construct_response(result)


@files.route('/preview/<file_id>', methods=['GET'])
def preview(file_id):

    values = dict()
    values["id"] = file_id
    result = filesService.preview(values)
    return default_response.construct_image_response(location=result)


filesService = FilesService([])
