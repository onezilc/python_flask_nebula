
import time
import os
from src import common, enums
from utils.initApp import mysql, app
# from src.qiniuClass import Qiniu


# class UserService(Qiniu):
class UserService():
    def __init__(self, describe):
        # super(UserService, self).__init__()
        self.totalStorage = 10 * 1024 * 1024 * 1024
        self.user_describe = describe
        self.avatar_path = os.getcwd() + "/uploadAvatars/"
        return

    def __construct_user_info(self, user):

        data = dict()
        data['userName'] = user['userName']
        data['userId'] = user['userId']
        data['time'] = str(time.time())

        info = dict()
        info['token'] = common.construct_token(data)
        # add_user_token(_data['username'], _data['token'])
        return info

    def __construct_avatar_path(self, values):
        user_id = values["userId"]
        upload_path = self.avatar_path
        user_path = upload_path + user_id
        if not os.path.exists(user_path):
            os.makedirs(user_path)
        return user_path

    def __save_avatar(self, file_data, values):
        user_path = self.__construct_avatar_path(values)
        upload_path = os.path.join(
            user_path, '{}.{}'.format(values['id'], values['name']))
        chunk = open(upload_path, 'ab+')
        chunk.write(file_data.stream.read())
        chunk.close()

    def list_all(self):
        result = mysql.list_all_filter('user', self.user_describe)
        return result

    def avatar(self, values):
        user = mysql.get('user', values, cur_key='userId')
        sql = mysql._concat(
            " SELECT SUM({}) AS {} ".format('size', 'size'),
            ' FROM {} '.format('files'),
            'WHERE ',
            ' {}= "{}"' .format('userId', values['userId']),
            ' AND {}= "{}"' .format(
                'isDelete', enums.IS_DELETE_CODE_MAP['FALSE'])
        )
        data = mysql.customize_sql(sql)

        result = dict()
        result['avatar'] = user['avatar']
        result['userName'] = user['userName']
        result['userId'] = user['userId']
        result['storage'] = int(data[0]['size']) if data[0]['size'] else 0
        result['totalStorage'] = self.totalStorage
        return result

    def get(self, values):
        user = mysql.get('user', values, cur_key='userId')
        result = common.filter_key(user, self.user_describe)
        return result

    def update(self, values):
        result = mysql.update("user", values, cur_key="userId")
        return result

    def reset(self, values):
        sha256_old_password = common.make_sha256(values['oldPassword'])
        sha256_new_password = common.make_sha256(values['newPassword'])

        user = mysql.get('user', values, cur_key='userId')

        if user['password'] == sha256_old_password:
            data = dict()
            data['userId'] = values['userId']
            data['password'] = sha256_new_password
            user = mysql.update('user', data, cur_key='userId')
            return {}, '修改成功', 0
        else:
            return {}, '旧密码输入有误', -1

    def active(self ,values):
        sha256_password = common.make_sha256(values['password'])
        user = mysql.get('user', values, cur_key='userId')

        if user['actived'] == enums.ACTIVED_CODE_MAP['TRUE']:
            return {}, '操作失败,该账户已激活', -1
        else:
            data = dict()
            data['userId'] = values['userId']
            data['actived'] = enums.ACTIVED_CODE_MAP['TRUE']
            data['password'] = sha256_password
            user = mysql.update('user', data, cur_key='userId')
            return {}, '账户激活成功', 0

    def uploadAvatar(self, file_data, values):

        values['id'] = common.create_id()

        self.__save_avatar(file_data, values)

        params = dict()
        params['avatar'] = 'http://{}:{}/user/image/{}/{}.{}'.format(
            app.config["SQL_ADDRESS"],
            app.config['APP_PORT'],
            values['userId'],
            values['id'],
            values['name'])
        params['userId'] = values['userId']
        mysql.update('user', params, cur_key='userId')
        return {}
