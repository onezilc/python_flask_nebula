

* 启动(虚拟环境)

~~~
virtualenv --no-site-packages evnv

source ./evnv/bin/activate

pip3 install  -i https://pypi.tuna.tsinghua.edu.cn/simple  -r requirements.txt
~~~



* vscode

  ~~~
  {
      "python.venvPath": "./evnv",
      "python.pythonPath": "./evnv/bin/python3.7",
      "python.formatting.provider": "autopep8",
      "workbench.colorTheme": "Default Dark+"
  }
  ~~~

  