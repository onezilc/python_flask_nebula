
import os
import time
from datetime import datetime
from src import common, enums
from utils.initApp import mysql
from ..user.service import UserService


class FilesService(UserService):
    def __init__(self, describe):
        super(FilesService, self).__init__([])
        self.file_describe = describe
        self.files_path = os.getcwd() + "/uploadFiles/"
        return

    def __add_file(self, values):
        _result = mysql.get('files', values, cur_key='id')
        if _result:
            return False
        else:
            mysql.insert('files', values)
            self.__update_nodes_ref(values)
            return True

    def __update_nodes_ref(self, values):
        print(values)
        if(values['parentId'] == '-1'):
            values['nodesRef'] = "-1,{}".format(values['id'])
        else:
            params = dict()
            params['id'] = values['parentId']
            parent = mysql.get('files', params)
            values['nodesRef'] = '{},{}'.format(
                parent['nodesRef'], values['id'])
        mysql.update('files', values)

    def __update_use(self):
        sql = mysql._concat(
            ' INSERT INTO ',
            ' {} ({},{})  '.format('statistics', 'userId', 'total'),
            ' SELECT {}, SUM({}) AS {} '.format('userId', 'size', 'total'),
            ' from {} '.format('files'),
            ' WHERE ',
            ' {} ="{}" '.format('isDelete', enums.IS_DELETE_CODE_MAP['FALSE']),
            ' AND {} ="{}" '.format('userId', common.get_token()['userId'])
        )
        # mysql.customize_sql(sql)

    def __construct_user_path(self, values):
        user_id = values["userId"]
        upload_path = self.files_path
        user_path = upload_path + user_id
        if not os.path.exists(user_path):
            os.makedirs(user_path)
        return user_path

    def __construct_cache_path(self, values):
        user_path = self.__construct_user_path(values)
        file_flag = values['flag']
        file_type = values['type']
        cache_name = "%s.%s.uploading" % (file_flag, file_type)
        path = os.path.join(user_path, cache_name)
        return path

    def __calc_table(self, values):
        table = values["table"]
        index = int(values["index"])

        _table = list(table)
        _table[index] = '1'
        _table = "".join(_table)
        return _table

    def __write_chunk(self, file_data, values):
        path = self.__construct_cache_path(values)
        index = int(values["index"])
        chunk_size = int(values["index"])
        table = values["table"]
        done = table.count("0") == 1 and table.find("0") == index
        offset = chunk_size * index

        chunk = open(path, 'ab+')
        chunk.seek(offset)
        chunk.write(file_data.stream.read())
        chunk.close()
        return done

    def __rename_chunk(self, values):
        cache_path = self.__construct_cache_path(values)
        old_name = os.path.splitext(cache_path)
        os.rename(cache_path, old_name[0])
        return old_name[0]

    def __download_piece(self, values):
        record = mysql.get('files', values, "id")
        size = int(record["size"])
        location = record["location"]
        ranges = values["ranges"]
        start = int(ranges['start']) if ranges['start'] else 0
        start = 0 if start < 0 else start
        end = int(ranges['end']) if ranges['end'] else size
        end = size if end > size else end

        file = open(location, 'rb')
        file.seek(start)
        piece = file.read(end - start + 1)
        file.close()

        result = dict()
        result['name'] = record["name"]
        result['start'] = start
        result['end'] = end
        result['size'] = size
        result["piece"] = piece
        # time.sleep(3)
        return result

    def __download_whole(self, values):
        result = mysql.get('files', values, "id")
        result["start"] = "0"
        result["end"] = result["size"]
        return result

    def __get_select_nodes(self, ids):
        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE {} '.format('id'),
            ' IN {} '.format(tuple(ids)),
        )
        return mysql.customize_sql(sql)

    def __get_all_nodes(self, nodes):
        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE ',
            ' OR '.join(map(lambda node: '{} LIKE "{}%"'.format(
                'nodesRef', node['nodesRef']), nodes)),
        )
        return mysql.customize_sql(sql)

    def __get_paths(self, node_id, values):
        ref = '-1'
        path_map = {'-1': '全部文件'}

        if(values['name']):
            return [{
                'id': '', 'name': '全部文件'},
                {'id': '0', 'name': '搜索 "{}" '.format(values['name'])}
            ]
        sql = mysql._concat(
            ' SELECT {} '.format('nodesRef'),
            ' FROM {} '.format('files'),
            ' WHERE {} = "{}" '.format('id', node_id),
        )
        node = mysql.customize_sql(sql)

        if(node):
            ref = node[0]['nodesRef']
            sql = mysql._concat(
                ' SELECT {},{} '.format('name', 'id'),
                ' FROM {} '.format('files'),
                ' WHERE',
                ' LOCATE ( {} , "{}" ) > 0 '.format('id', ref)
            )
            paths = mysql.customize_sql(sql)
            for path in paths:
                path_map[path['id']] = path['name']

        path_list = list(map(lambda file_id: {
            'id': file_id,
            'name': path_map[file_id]
        }, ref.split(',')))

        return path_list

    def upload(self, file, values):
        table = self.__calc_table(values)
        done = self.__write_chunk(file, values)

        if done:
            record = dict()
            record["userId"] = values["userId"]
            record["id"] = common.create_id()
            record["name"] = values["name"]
            record["parentId"] = values["parentId"]
            record["type"] = values["type"]
            record["size"] = values["size"]
            record["location"] = self.__rename_chunk(values)
            self.__add_file(record)
            self.__update_use()

        result = dict()
        result["index"] = int(values["index"])
        result['flag'] = values["flag"]
        result['table'] = table

        print(result)
        # time.sleep()
        return result

    def download(self, values):
        if(values["ranges"]):
            print('断点下载模式')
            result = self.__download_piece(values)
            return result, 206
        else:
            print('普通下载模式')
            result = self.__download_whole(values)
            return result, 200

    def create(self, values):
        if(not values['name']):
            return {}, '名称不能为空', -1
        else:
            result = self.__add_file(values)
        return result, '', 0

    def list_all(self, values):

        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE {} = "{}" '.format('userId', values['userId']),
            ' AND {} = "{}" '.format(
                'isDelete', enums.IS_DELETE_CODE_MAP['FALSE']),
            ' AND {} = "{}" '.format('parentId', values['parentId']) if(
                not values['name']) else '',
            ' AND {} = "{}" '.format('type', values['type']) if(
                values['type']) else '',
            ' AND {} LIKE "%{}%" '.format(
                'name', values['name']) if(values['name']) else '',
            ' ORDER BY {} {} '.format(values['order'], values['sort']),
        )
        file_list = mysql.customize_sql(sql)
        path_list = self.__get_paths(values['parentId'], values)
        result = dict()
        result['list'] = file_list
        result['paths'] = path_list

        return result

    def rename(self, values):

        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE ',
            ' {} = "{}" '.format('parentId', values['parentId']),
            ' AND',
            ' {} = "{}" '.format('name', values['name']),
            ' AND NOT ',
            ' {} = "{}" '.format('id', values['id'])
        )
        record = mysql.customize_sql(sql)

        if not values['name']:
            return {}, '名称不能为空', -1
        if record:
            return {}, '文件名已存在', -1
        else:
            result = mysql.update('files', values)
        return result, 'ok', 0

    def remove(self, values):
        ids = values['ids'] + ['']

        select_nodes = self.__get_select_nodes(ids)

        # mysql.multi_delete('share', values, filter_key="fileId")

        all_nodes = self.__get_all_nodes(select_nodes)

        ids = list(map(lambda node: node['id'], all_nodes))
        print(ids)
        # 删除分享数据(外键关联)
        mysql.multi_delete('share', {'ids': ids}, filter_key="fileId")

        # 删除文件数据本身
        sql = mysql._concat(
            ' DELETE ',
            ' FROM {} '.format('files'),
            ' WHERE ',
            ' OR '.join(map(lambda node: "nodesRef LIKE '{}%'".format(
                node['nodesRef']), select_nodes))
        )
        mysql.customize_sql(sql)

        return {}

    def recover(self, values):
        ids = values['ids'] + ['']

        self.move(values)

        select_nodes = self.__get_select_nodes(ids)

        # 根据选中ref 把所有子节点置 delete
        sql = mysql._concat(
            ' UPDATE {} '.format('files'),
            ' SET ',
            ' {} = "{}" '.format(
                'isDelete', enums.IS_DELETE_CODE_MAP['FALSE']),
            ' WHERE ',
            ' OR '.join(map(lambda node: "nodesRef LIKE '{}%'".format(
                node['nodesRef']), select_nodes))
        )
        mysql.customize_sql(sql)
        self.__update_use()
        return {}

    def delete(self, values):
        ids = values['ids'] + ['']

        params = {'targetId': '-1', 'ids': values['ids']}

        self.move(params)

        select_nodes = self.__get_select_nodes(ids)

        # 根据选中ref 把所有子节点置 delete
        sql = mysql._concat(
            ' UPDATE {} '.format('files'),
            ' SET ',
            ' {} = "{}" '.format('isDelete', enums.IS_DELETE_CODE_MAP['TRUE']),
            ' WHERE ',
            ' OR '.join(map(lambda node: "nodesRef LIKE '{}%'".format(
                node['nodesRef']), select_nodes))
        )
        mysql.customize_sql(sql)
        self.__update_use()
        return {}

    def move(self, values):
        print(values)
        ids = values['ids'] + ['']

        target = mysql.get('files', {'id': values['targetId']})
        target = target or {'nodesRef': "-1"}

        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE {} '.format('id'),
            ' IN {} '.format(tuple(ids)),
            ' AND ',
            ' LOCATE ( {} , "{}" ) > 0 '.format('nodesRef', target['nodesRef'])
        )
        contain = mysql.customize_sql(sql)

        if(not contain):

            # 更新所选 文件的父节点
            sql = mysql._concat(
                ' UPDATE {} '.format('files'),
                ' SET ',
                ' {} = "{}" '.format('parentId', values['targetId']),
                ' WHERE {} '.format('id'),
                ' IN {} '.format(tuple(ids))
            )
            mysql.customize_sql(sql)

            select_nodes = select_nodes = self.__get_select_nodes(ids)

            # 更新所选文件以及子文件的索引
            sql = mysql._concat(
                ' UPDATE {} '.format('files'),
                ' SET ',
                ' {} = CASE '.format('nodesRef'),
                ' '.join(map(
                    lambda select_node: mysql._concat(
                        ' WHEN {} LIKE "{}%" '.format(
                            'nodesRef', select_node['nodesRef']),
                        ' THEN REPLACE({}, "{}", CONCAT("{}",",","{}")) '.format(
                            'nodesRef', select_node['nodesRef'], target['nodesRef'], select_node['id'])
                    ), select_nodes)),
                ' ELSE {} '.format('nodesRef'),
                ' END ',
            )
            mysql.customize_sql(sql)

            return {}, '', 0
        else:
            return {}, '不能将文件移动到自身或是其子目录下', -1

    def detail(self, values):
        file_id = values['id']
        filter_keys = ['files.size', 'files.type', 'files.nodesRef',
                       'files.createTime', 'files.updateTime',
                       'files.name', 'files.userId', 'user.userName']
        conditions = ['files.userId = user.userId']
        where = ['files.id = "{}"'.format(
            file_id)]
        share_record = mysql.get('share', {'fileId': file_id}, 'fileId')

        result = mysql.list_left_join_by(
            left_table_name='files', right_table_name='user', filter_keys=filter_keys, conditions=conditions, where=where)
        result = result and result[0]

        result['updateTime'] = str(result['updateTime'])
        result['createTime'] = str(result['createTime'])

        if(share_record):
            result['shareCode'] = share_record['shareCode']
            result['shareType'] = share_record['shareType']

        sql = "SELECT SUM(size) AS size FROM files WHERE nodesRef LIKE '{}%'".format(
            result['nodesRef'])

        data = mysql.customize_sql(sql)
        result['size'] = int(data[0]['size']) if data[0]['size'] else 0

        return result

    def copy(self, values):

        params = mysql.get('files', values)
        params['id'] = common.create_id()
        params['userId'] = values["userId"]
        params['parentId'] = values['targetId']
        params.pop('createTime')
        params.pop('updateTime')

        self.create(params)
        self.__update_use()
        return {}

    def list_garbage(self, values):

        sql = mysql._concat(
            ' SELECT {} '.format('*'),
            ' FROM {} '.format('files'),
            ' WHERE {} = "{}" '.format('userId', values['userId']),
            ' AND {} = "{}" '.format(
                'isDelete', enums.IS_DELETE_CODE_MAP['TRUE']),
            ' AND {} = "{}" '.format('parentId', values['parentId']) if(
                not values['name']) else '',
            ' AND {} = "{}" '.format('type', values['type']) if(
                values['type']) else '',
            ' AND {} LIKE "%{}%" '.format(
                'name', values['name']) if(values['name']) else '',
            ' ORDER BY {} {} '.format(values['order'], values['sort']),
        )
        file_list = mysql.customize_sql(sql)
        path_list = self.__get_paths(values['parentId'], values)

        result = dict()
        result['list'] = file_list
        result['paths'] = path_list

        return result

    def statistics(self, values):
        sql = mysql._concat(
            ' SELECT {} ,'.format('type'),
            ' SUM({}) AS {} '.format('size', 'typeSize'),
            ' FROM {} '.format('files'),
            ' WHERE ',
            ' {}="{}" '.format('userId', values['userId']),
            ' AND ',
            ' {}="{}" '.format('isDelete', enums.IS_DELETE_CODE_MAP['FALSE']),
            ' AND ',
            ' {}>"{}" '.format('size', '0'),
            ' GROUP BY {} '.format('type')
        )
        group = mysql.customize_sql(sql)

        sql = mysql._concat(
            ' SELECT {} , {} '.format('time', 'total'),
            ' FROM {} '.format('statistics'),
            ' WHERE ',
            ' DATE_SUB(CURDATE(), INTERVAL 7 DAY)<= date({}) '.format('time'),
            ' AND ',
            ' {}="{}" '.format('userId', values['userId']),
            ' ORDER BY {} {} '.format('time', 'asc'),
        )
        use = mysql.customize_sql(sql)

        result = dict()
        result['total'] = self.totalStorage
        result['group'] = group
        result['use'] = use
        return result

    def preview(self, values):
        data = mysql.get('files', values)
        return data['location']
