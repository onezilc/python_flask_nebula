
import requests
import json
from flask import redirect, jsonify,session
from utils.initApp import mysql, app
from furl import furl
from src import common,enums
from ....auth.service import AuthService

class GithubService(AuthService):
    def __init__(self, describe):
        self.client_id = app.config['GITHUB_CLIENT_ID']
        self.client_secret = app.config['GITHUB_CLIENT_SECRET']
        self.base_url = 'http://{}:{}/auth/oauth/github'.format(app.config["SQL_ADDRESS"],app.config["APP_PORT"])
        return

    def __create_github_user(self,values):
        user = mysql.get('user',{'githubId': values['id']},'githubId')
        if(not user):
            user = dict()
            user['userId'] = common.create_id()
            user['userName'] = '{}_{}'.format(values['login'],common.get_ranstr(6))
            user['avatar'] = values['avatar_url']
            user['sign'] = values['html_url']
            user['githubId'] = values['id']
            user['email'] = values['email'] or 'expample@expample.com'
            user['actived'] = enums.ACTIVED_CODE_MAP["FALSE"]

            mysql.insert('user',user)
        return user   

    def login(self, values):
        login_url = 'https://github.com/login/oauth/authorize'
        params = {
            'client_id': self.client_id,
            # 如果不填写redirect_uri那么默认跳转到oauth中配置的callback url。
            'redirect_uri': '{}/callback'.format(self.base_url),
            'scope': 'read:user',
            # 随机字符串，防止csrf攻击
            'state': 'An unguessable random string.',
            # 'allow_signup': 'true'
        }
        print(params)
        url  = furl(login_url).set(params)
        return url, 302

    def callback(self):
        return {},'授权成功,正在获取登录信息...',0

    def access(self,values):
        access_token_url = 'https://github.com/login/oauth/access_token'
        access_user_url = 'https://api.github.com/user'
        
        payload = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': values['code'],
            'state': 'An unguessable random string.'
        }

        r = requests.post(access_token_url, json=payload,
                          headers={'Accept': 'application/json'})
        access_token = json.loads(r.text).get('access_token')
        
        r = requests.get(access_user_url, headers={
                         'Authorization': 'token ' + access_token})

        result = json.loads(r.text)
        user = self.__create_github_user(result)

        session['userName'] = user['userName']
        data = self._construct_user_info(user)
        return data

