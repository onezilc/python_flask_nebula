/*
 Navicat Premium Data Transfer

 Source Schema         : nebula

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentId` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) CHARACTER SET latin1 DEFAULT 'folder',
  `size` int(255) unsigned zerofill DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isDelete` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `nodesRef` varchar(255) COLLATE utf8_unicode_ci DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `files_f_userId` (`userId`),
  FULLTEXT KEY `files_ft_name` (`name`),
  CONSTRAINT `files_f_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for reset
-- ----------------------------
DROP TABLE IF EXISTS `reset`;
CREATE TABLE `reset` (
  `no` int(255) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `token` longtext COLLATE utf8_unicode_ci,
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for share
-- ----------------------------
DROP TABLE IF EXISTS `share`;
CREATE TABLE `share` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fileId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shareCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shareType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `share_f_userId` (`userId`),
  KEY `share_f_fileId` (`fileId`),
  CONSTRAINT `share_f_fileId` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`),
  CONSTRAINT `share_f_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for statistics
-- ----------------------------
DROP TABLE IF EXISTS `statistics`;
CREATE TABLE `statistics` (
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` int(11) DEFAULT NULL,
  `useId` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`useId`),
  KEY `use_f_userId` (`userId`),
  CONSTRAINT `use_f_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=10477 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userId` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `githubId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actived` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`userId`,`userName`) USING BTREE,
  UNIQUE KEY `unique` (`userName`),
  KEY `userId` (`userId`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Event structure for e_delete_timeout
-- ----------------------------
DROP EVENT IF EXISTS `e_delete_timeout`;
delimiter ;;
CREATE EVENT `e_delete_timeout`
ON SCHEDULE
EVERY '1' DAY STARTS '2020-04-04 10:47:22'
DO delete from files where updateTime < (CURRENT_TIMESTAMP() + INTERVAL - 15 DAY) and isDelete ='1'
;
;;
delimiter ;

-- ----------------------------
-- Event structure for e_statistics_total
-- ----------------------------
DROP EVENT IF EXISTS `e_statistics_total`;
delimiter ;;
CREATE EVENT `e_statistics_total`
ON SCHEDULE
EVERY '1' HOUR STARTS '2020-04-04 10:47:22'
DO INSERT INTO 

statistics (userId,total) 

 SELECT userId, SUM(size) AS total
    FROM files
		WHERE isDelete ='0'
    GROUP BY userId
;
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
