from flask import Blueprint, request, jsonify
from src.nestableBlueprintClass import NestableBlueprint

from .github.controller import github

oauth = NestableBlueprint("oauth", __name__, url_prefix="/oauth")

oauth.register_blueprint(
    github
)
