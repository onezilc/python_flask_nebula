import json
from flask import request, jsonify, session, redirect

from src import common
from utils.initApp import default_response, app
from utils.checkAuth import check_auth, generate_auth, clear_auth

from . import github
from .service import GithubService


@github.route("/login", methods=['GET'])
def login():

    values = dict()
    url, code = githubService.login(values)
    return redirect(url, code)


@github.route('/callback', methods=['GET'])
def callback():
    result,msg,code = githubService.callback()
    return default_response.construct_response(result,msg,code)


@github.route('/access/<code>', methods=['GET'])
@generate_auth
def access(code):
    values = dict()
    values['code'] = code
    result = githubService.access(values)
    return default_response.construct_response(result)

githubService = GithubService([])
