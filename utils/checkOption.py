
from flask import request

from utils.initApp import app, default_response


@app.before_request
def check_option():
    if request.method == "OPTIONS":
        return default_response.construct_response("")
