import json
from urllib.parse import quote
from flask import request, jsonify

from utils.initApp import mysql, default_response
from utils.checkAuth import check_auth
from src import common

from . import share
from .service import ShareService


@share.route('/add', methods=['POST'])
@check_auth
def add():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['fileId'] = data['fileId']
    values['type'] = data['type']
    values['userId'] = common.get_token()['userId']

    result = shareService.add(values)
    return default_response.construct_response(result)


@share.route('/verify', methods=['POST'])
@check_auth
def verify():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['id'] = data['id']
    values['shareCode'] = data['shareCode']

    result, msg, code = shareService.verify(values)
    return default_response.construct_response(result, msg, code)


@share.route('/delete/<share_id>', methods=['GET'])
@check_auth
def delete(share_id):

    values = dict()
    values['id'] = share_id

    result = shareService.delete(values)
    return default_response.construct_response(result)


@share.route('/resource/<share_id>', methods=['GET'])
def resource(share_id):

    values = dict()
    values['id'] = share_id
    values["ranges"] = common.get_ranges()

    result, code = shareService.resource(values)
    return default_response.construct_file_response(result), code


@share.route('/listAuth', methods=['GET'])
@check_auth
def list_auth():

    values = dict()
    values['name'] = request.args.get('name')
    values['userId'] = common.get_token()['userId']

    result = shareService.list_auth(values)
    return default_response.construct_response(result)


@share.route('/listPage', methods=['GET'])
@check_auth
def list_page():

    values = dict()
    values['name'] = request.args.get('name')

    result = shareService.list_page(values)
    return default_response.construct_response(result)

@share.route('/save', methods=['POST'])
@check_auth
def save():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values["targetId"] = data['parentId']
    values["id"] = data['shareId']
    values["userId"] = common.get_token()["userId"]

    result = shareService.save(values)
    return default_response.construct_response(result)

shareService = ShareService(
    ['id', "userId", "type", "updateTime", "shareCode"])
