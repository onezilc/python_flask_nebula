from flask import request, abort, session
from functools import wraps
from utils.initApp import app
from datetime import timedelta


def generate_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session.permanent = True
        app.permanent_session_lifetime = timedelta(
            seconds=1000)  # 设置session到期时间
        return func(*args, **kwargs)
    return wrapper


def clear_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session.clear()
        return func(*args, **kwargs)
    return wrapper


def check_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not session.get('userName'):
            abort(403)
            # return func(*args, **kwargs)
        else:
            return func(*args, **kwargs)
    return wrapper
