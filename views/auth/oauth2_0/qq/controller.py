import json
from flask import request, jsonify, session, redirect

from src import common
from utils.initApp import default_response, app
from utils.checkAuth import check_auth, generate_auth, clear_auth

from . import qq
from .service import QQService


@qq.route("/login", methods=['GET'])
def login():

    values = dict()
    url, code = qqService.login(values)
    return redirect(url, code)

@qq.route('/callback', methods=['GET'])
def callback():
    result,msg,code = qqService.callback()
    return default_response.construct_response(result,msg,code)

@qq.route('/access/<code>', methods=['GET'])
@generate_auth
def access(code):
    values = dict()
    values['code'] = code
    result = qqService.access(values)
    return default_response.construct_response(result)

qqService = QQService([])
