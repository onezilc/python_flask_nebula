import json
from flask import request, jsonify, session

from src import common
from utils.initApp import default_response, app
from utils.checkAuth import check_auth, generate_auth, clear_auth

from . import auth
from .service import AuthService


@auth.route("/login", methods=['POST'])
@generate_auth
def login():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['userName'] = data["userName"]
    values['password'] = data["password"]

    result, msg, code = authService.login(values)
    return default_response.construct_response(result, msg, code)


@auth.route("/logout", methods=['GET'])
@check_auth
@clear_auth
def logout():

    result = authService.logout()
    return default_response.construct_response(result)


@auth.route("/register", methods=['POST'])
def register():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values["userName"] = data["userName"]
    values['password'] = data["password"]
    values["email"] = data["email"]

    result = authService.register(values)
    return default_response.construct_response(result)


@auth.route("/forgot", methods=['POST'])
def forgot():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values["email"] = data["email"]

    result, msg, code = authService.forgot(values)
    return default_response.construct_response(result, msg, code)


@auth.route("/reset/<reset_token>", methods=['GET'])
def reset(reset_token):

    values = dict()
    values["token"] = reset_token

    result = authService.reset(values)
    return result


authService = AuthService(['username', 'id'])
