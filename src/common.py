import hashlib
import base64
import json
import time
import re
import random
import datetime
from flask import request, abort
from . import app


def make_sha1(string, encode='utf-8'):
    _hash = hashlib.sha1()
    _hash.update(string.encode(encode))
    _result = _hash.hexdigest()
    return _result


def make_md5(string, encode='utf-8'):
    _hash = hashlib.md5()
    _hash.update(string.encode(encode))
    _result = _hash.hexdigest()
    return _result


def make_base64_encode(string, encode='utf-8'):
    _bytes = base64.b64encode(string.encode(encode))
    _str = str(_bytes, encode)
    return _str


def make_base64_decode(string, encode='utf-8'):
    _bytes = base64.b64decode(string.encode(encode))
    _str = str(_bytes, encode)
    return _str


def make_sha256(string, encode='utf-8'):
    _hash = hashlib.sha256()
    _hash.update(string.encode(encode))
    _result = _hash.hexdigest()
    return _result


def filter_key(dic, keys):
    _dic = dict()
    for cur in dic:
        if cur in keys:
            _dic[cur] = dic[cur]
    return _dic


def create_id():
    m = hashlib.md5(str(time.clock()).encode('utf-8'))
    return m.hexdigest()


def construct_token(data):
    header = json.dumps({
        'type': 'JWT',   # 类型是JWT,固定值
        'alg': 'HS256'    # 签名算法, 可以使用其它算法, 相应的影响signature的计算
    })
    payload = json.dumps(data)

    encoded_header = make_base64_encode(header)
    encoded_payload = make_base64_encode(payload)
    signature = make_sha256("{}.{}.{}".format(
        encoded_header,
        encoded_payload,
        app.config["PRIVATE_KEY"]
    ))
    token = "{}.{}.{}".format(
        encoded_header,
        encoded_payload,
        signature
    )
    return token


def parse_token(token):
    encoded_header, encoded_payload, signature = token.split('.')

    _signature = make_sha256("{}.{}.{}".format(
        encoded_header,
        encoded_payload,
        app.config["PRIVATE_KEY"]
    ))

    if(signature == _signature):
        return json.loads(make_base64_decode(encoded_payload))
    else:
        pass


def get_token():
    token = request.headers.get('Token')
    if not token:
        abort(401)
    else:
        try:
            token = parse_token(token)
            if not token:
                abort(403)
        except Exception:
            abort(403)
    return token


def get_ranges():
    _range = request.headers.get('Range')
    print(_range)
    if not _range:
        return
    else:
        v = re.match(r'bytes=(\d+)-((\d+)?)', _range)
        return {
            "start": v.group(1),
            "end": v.group(2)
        }


def get_ranstr(num):
    H = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    salt = ''
    for i in range(num):
        salt += random.choice(H)
    return salt


def get_now_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
