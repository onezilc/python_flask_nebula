from flask import Blueprint, request, jsonify
from src.nestableBlueprintClass import NestableBlueprint

from .oauth2_0 import oauth

auth = NestableBlueprint("auth", __name__, url_prefix="/auth")

auth.register_blueprint(
    oauth
)
