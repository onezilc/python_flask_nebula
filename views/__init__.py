from utils import initApp
from utils import errorCatcher
from utils import checkOption

from .auth.controller import auth
from .files.controller import files
from .share.controller import share
from .user.controller import user

app = initApp.app


blueprints = [
    auth,
    files,
    share,
    user,
]


def register_blueprint(_blueprints):
    for cur in _blueprints:
        app.register_blueprint(cur)  # 注册蓝图


register_blueprint(blueprints)
