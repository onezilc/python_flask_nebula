FROM python:3.7-alpine
LABEL maintainer="dywzzj@163.com"
# ENV PIPURL "http://pypi.mirrors.ustc.edu.cn/simple/"
# ENV PIPURL "https://mirrors.aliyun.com/pypi/simple/"
# ENV PIPURL "https://pypi.tuna.tsinghua.edu.cn/simple"


RUN apk add --no-cache gcc musl-dev

WORKDIR /flask

COPY ./ ./

RUN pip --no-cache-dir install --upgrade pip
RUN pip --no-cache-dir install -r requirements.txt

# CMD gunicorn  -c gun.conf flaskr:appdocker 
CMD python3 flaskr.py