import sys
import views
import time

app = views.app  # 初始化app

if __name__ == '__main__':
    app.run(
        host=app.config['APP_HOST'],
        port=app.config['APP_PORT'],
        debug=app.config['APP_DEBUG'])
