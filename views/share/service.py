
from src import common, enums
from utils.initApp import mysql

from ..files.service import FilesService


class ShareService(FilesService):
    def __init__(self, describe):
        super(ShareService, self).__init__([])
        self.share_describe = describe
        return

    def add(self, values):
        share = mysql.get_by('share', values, condition_keys=[
                             'userId', "fileId"])

        result = dict()
        if(share):
            data = dict()
            data['id'] = share['id']
            data['shareCode'] = '' if values['type'] == enums.SHARE_TYPE_CODE_MAP.get('PUBLIC') else common.get_ranstr(
                6)
            data['shareType'] = values['type']

            mysql.update('share', data)
            result['shareCode'] = data['shareCode']
        else:
            data = dict()
            data['id'] = common.create_id()
            data['shareCode'] = '' if values['type'] == enums.SHARE_TYPE_CODE_MAP.get('PUBLIC') else common.get_ranstr(
                6)
            data['shareType'] = values['type']
            data['fileId'] = values['fileId']
            data['userId'] = values['userId']

            mysql.insert('share', data)
            result['shareCode'] = data['shareCode']
        return result

    def verify(self, values):
        share_record = mysql.get('share', values)

        if(share_record['shareCode'] == values['shareCode']):
            return {}, '验证通过', 0
        else:
            return {}, '邀请码错误', -1

    def resource(self, values):
        share_record = mysql.get('share', values)

        data = dict()
        data['id'] = share_record['fileId']
        data['ranges'] = values["ranges"]

        return self.download(data)

    def delete(self, values):
        result = mysql.delete('share', values)
        return result

    def list_auth(self, values):
        table_names = ['share', 'files', 'user']
        filter_keys = ['files.size', 'files.type', 'files.name', 'share.shareCode', 'share.shareType',
                       'share.id', 'share.userId', 'user.userName']
        conditions = ['share.fileId = files.id', 'share.userId = user.userId',
                      'user.userId = "{}"'.format(values['userId']),
                      'files.isDelete = "{}"'.format(enums.IS_DELETE_CODE_MAP.get("FALSE"))]
        if(values['name']):
            conditions.append("files.name like '%{}%'".format(values['name']))
        
        result = mysql.list_inner_join_by(
            table_names=table_names, filter_keys=filter_keys, conditions=conditions)
        return result

    def list_page(self, values):
        table_names = ['share', 'files', 'user']
        filter_keys = ['files.size', 'files.type', 'files.name', 'share.shareType',
                       'share.id', 'share.userId', 'user.userName']
        conditions = ['share.fileId = files.id', 'share.userId = user.userId',
                      'files.isDelete = "%s"' % enums.IS_DELETE_CODE_MAP.get("FALSE")]

        if(values['name']):
            conditions.append("files.name like '%{}%'".format(values['name']))

        result = mysql.list_inner_join_by(
            table_names=table_names, filter_keys=filter_keys, conditions=conditions)
        return result

    def save(self, values):

        record = mysql.get('share', values)

        params = mysql.get('files', {'id': record['fileId']})
        params['id'] = common.create_id()
        params['userId'] = values["userId"]
        params['parentId'] = values['targetId']
        params.pop('createTime')
        params.pop('updateTime')

        self.create(params)

        return {}
