
import os
import sys
from datetime import timedelta
from . import app

debug = len(sys.argv) == 2 and sys.argv[1] == 'dev'

# app运行参数
app.config['APP_DEBUG'] = debug
app.config["APP_HOST"] = '0.0.0.0'
app.config["APP_PORT"] = '9009'
# 数据库配置
app.config["SQL_ADDRESS"] = '127.0.0.1'
app.config["SQL_DATABASE"] = 'nebula'
app.config["SQL_USER"] = 'root'
app.config["SQL_PASSWORD"] = ''
app.config["SQL_PORT"] = 3306
# token配置
app.config["TIMEOUT_TOKEN"] = 10 * 24 * 60 * 60
app.config["TIMEOUT_EMAIL"] = 10 * 60
# 私钥
app.config["PRIVATE_KEY"] = "19980820"
# 邮件配置
app.config["MAIL_SERVER"] = 'smtp.163.com'
app.config["MAIL_PORT"] = 25
app.config["MAIL_USE_TLS"] = True
app.config["MAIL_USERNAME"] = ''
app.config["MAIL_PASSWORD"] = ''
# session
app.config['SECRET_KEY'] = 'session_secret_key'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(hours=2)
# 七牛云
app.config['QINIU_ACCESS_KEY'] = 'jqjDwm2z10R6dF7gV1CsU6RIN2ceHMiB_WMMzdPO'
app.config['QINIU_SECRET_KEY'] = 'yYGGAZmqyO9TwfkCjFljvTAezXs3R7OmBSlu5hzh'
app.config['QINIU_BUCKET_NAME'] = '1zilc'
# github
app.config['GITHUB_CLIENT_ID'] = '595ec321d814f7bd6ee5'
app.config['GITHUB_CLIENT_SECRET'] = '4227cbe0416084c93fbf7e2e10ad9fd09b39eb30'