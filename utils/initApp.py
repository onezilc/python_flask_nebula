
from flask_cors import CORS
from flask_mail import Mail
from src import sqlClass
from src import responseClass
from src import config


app = config.app

# 添加cors
CORS(app, supports_credentials=True)

# 连接数据库，得到实例
mysql = sqlClass.Database(app)

# 得到邮件实例
mail = Mail(app)

# 得到响应实例
default_response = responseClass.CreateResponse()


# print(mysql.customize_sql( "select SQL_CALC_FOUND_ROWS * from files where name like %s%s%s limit 2,10" % ("'%",'.',"%'")))
# a = mysql.customize_sql( 'select FOUND_ROWS()')
# print(a)
