import json
from flask import request, jsonify
from src import common
from utils.initApp import default_response
from utils.checkAuth import check_auth

from . import user
from .service import UserService


@user.route('/listAll', methods=['GET'])
@check_auth
def list_all():

    result = userService.list_all()
    return default_response.construct_response(result)


@user.route('/avatar', methods=['GET'])
@check_auth
def avator():

    values = dict()
    values['userId'] = common.get_token()['userId']

    result = userService.avatar(values)
    return default_response.construct_response(result)


@user.route('/get/<user_id>', methods=['GET'])
@check_auth
def get(user_id):

    values = dict()
    values["userId"] = user_id

    result = userService.get(values)
    return default_response.construct_response(result)


@user.route('/update', methods=['POST'])
@check_auth
def update():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['email'] = data["email"]
    values['phone'] = data["phone"]
    values['gender'] = data["gender"]
    values['sign'] = data["sign"]
    values['userId'] = common.get_token()["userId"]

    result = userService.update(values)
    return default_response.construct_response(result)


@user.route('/reset', methods=['POST'])
@check_auth
def reset():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['userId'] = common.get_token()["userId"]
    values['oldPassword'] = data["oldPassword"]
    values['newPassword'] = data["newPassword"]

    result, msg, code = userService.reset(values)
    return default_response.construct_response(result, msg, code)

@user.route('/active', methods=['POST'])
@check_auth
def active():
    data = json.loads(request.get_data(as_text=True))

    values = dict()
    values['userId'] = common.get_token()["userId"]
    values['password'] = data["password"]

    result, msg, code = userService.active(values)
    return default_response.construct_response(result, msg, code)


@user.route('/uploadAvatar', methods=['POST'])
@check_auth
def uploadAvatar():

    values = dict()
    values['name'] = request.files.get('avatar').filename
    values['userId'] = common.get_token()['userId']
    print(request.files.get('avatar'))
    result = userService.uploadAvatar(request.files.get('avatar'), values)
    return default_response.construct_response(result)


@user.route('/image/<userId>/<path>', methods=['GET'])
@check_auth
def image(userId, path):
    return default_response.construct_image_response(userId, path)


userService = UserService(
    ['userName', 'userId', 'email', 'phone', 'gender', 'sign', 'avatar','actived'])
