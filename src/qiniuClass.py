
from qiniu import Auth, put_data, etag
from .config import app


class Qiniu:

    def __init__(self):
        self.access_key = app.config['QINIU_ACCESS_KEY']
        self.secret_key = app.config['QINIU_SECRET_KEY']
        self.bucket_name = app.config['QINIU_BUCKET_NAME']

    def upload_file_qiniu(self, inputdata):
        '''
        :param inputdata: bytes类型的数据
        :return: 文件在七牛的上传名字
        '''
        # 构建鉴权对象
        q = Auth(self.access_key, self.secret_key)

        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(self.bucket_name)
        
        # 如果需要对上传的图片命名，就把第二个参数改为需要的名字
        ret1, ret2 = put_data(token, None, data=inputdata)
        print('ret1:', ret1)
        print('ret2:', ret2)

        # 判断是否上传成功
        if ret2.status_code != 200:
            raise Exception('文件上传失败')

        return ret1.get('key')
