from utils.initApp import app, default_response
import datetime
import traceback


@app.errorhandler(Exception)
def error_handler(error):
    # 这个handler可以catch住所有的异常和raise exeception.
    msg = str(error) if not type(error) == 'str' else error
    code = error.code if hasattr(error,'code') else 500
    print("error",datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),error )  # 现在
    _response = default_response.construct_response(data=None, code=-1, msg=msg)
    return _response,code


# 自定义异常捕获
@app.errorhandler(401)
def error_401(error):
    # 这个handler可以catch住所有401以及找不到对应router的处理请求
    print(401, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),error)  # 现在
    _response = default_response.construct_response(data=None, code=error.code, msg='please login first ?')
    return _response, error.code


@app.errorhandler(404)
def error_404(error):
    # 这个handler可以catch住所有404
    print(404, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),error)  # 现在
    _response = default_response.construct_response(data=None, code=error.code, msg=error.description)
    return _response, error.code


@app.errorhandler(500)
def error_500(error):
    # 这个handler可以catch住所有401以及找不到对应router的处理请求
    print(500, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),error)  # 现在
    _response = default_response.construct_response(data=None, code=error.code, msg=error.description)
    return _response, error.code
